/**
 *
 */
package cern.accsoft.steering.jmad.modeldefs;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.modeldefs.create.AbstractLhcModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSet;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSetBuilder;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;

import java.util.ArrayList;
import java.util.List;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;
import static cern.accsoft.steering.jmad.modeldefs.create.OpticModelFileBuilder.modelFile;

public class LhcNominalModelDefinitionFactory extends AbstractLhcModelDefinitionFactory {

    @Override
    protected void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
        modelDefinition.addInitFile(new CallableModelFileImpl("toolkit/init-constants.madx"));
        modelDefinition.addInitFile(new CallableModelFileImpl("lhc.seq"));
    }

    @Override
    protected String getModelDefinitionName() {
        return "LHC 2025";
    }

    @Override
    protected List<OpticDefinitionSet> getOpticDefinitionSets() {
        List<OpticDefinitionSet> definitionSetList = new ArrayList<>();

        /* 2025 injection optics with Phase knob 25% */
        definitionSetList.add(createInjectionOptics_25Knob());

        /* 2025 injection optics with Phase knob 50% */
        definitionSetList.add(createInjectionOptics_50Knob());

        /* 2025 injection optics with Phase knob 75% */
        definitionSetList.add(createInjectionOptics_75Knob());

        /* 2025 injection optics with Phase knob 100% */
        definitionSetList.add(createInjectionOptics_100Knob());

        /* 2025 low beta optics */
        definitionSetList.add(createLowBetaOpticsSet());

        /* 2025 IONS optics */
        definitionSetList.add(createIonOpticsSet());

        return definitionSetList;
    }

    /**
     * ATS ramp and squeeze ... All at collision tune --> trimmed with knob to INJ (BP level)
     * @return
     */

    private OpticDefinitionSet createInjectionOptics_25Knob() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));

        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-phasechange-knobs.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/set-phasechange-knob-25strength.madx"));

        // 2025 optics
        builder.addOptic("R2025aRP_A11mC11mA10mL10m_PhaseKnob25ON", modelFile("strengths/ATS_Nominal/2025/ats_11m.madx"));

        return builder.build();
    }

    private OpticDefinitionSet createInjectionOptics_50Knob() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));

        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-phasechange-knobs.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/set-phasechange-knob-50strength.madx"));

        // 2025 optics
        builder.addOptic("R2025aRP_A11mC11mA10mL10m_PhaseKnob50ON", modelFile("strengths/ATS_Nominal/2025/ats_11m.madx"));

        return builder.build();
    }
    private OpticDefinitionSet createInjectionOptics_75Knob() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));

        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-phasechange-knobs.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/set-phasechange-knob-75strength.madx"));

        // 2025 optics
        builder.addOptic("R2025aRP_A11mC11mA10mL10m_PhaseKnob75ON", modelFile("strengths/ATS_Nominal/2025/ats_11m.madx"));

        return builder.build();
    }
    private OpticDefinitionSet createInjectionOptics_100Knob() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-phasechange-knobs.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/set-phasechange-knob-100strength.madx"));

        // 2025 optics
        builder.addOptic("R2025aRP_A11mC11mA10mL10m_PhaseKnob100ON", modelFile("strengths/ATS_Nominal/2025/ats_11m.madx"));

        return builder.build();
    }

//    private OpticDefinitionSet createInjectionOpticsWithNonATSKnobs() {
//        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();
//
//        /* initial optics strength files common to all optics (loaded before the other strength files) */
//        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
//        /* final optics strength files common to all optics (loaded after the other strength files) */
//        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
//        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
//        /* Define correct knobs to be used operationally */
//        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-non-ats.madx"));
//        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-non-ats.madx"));
//        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-non-ats.madx"));
//        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
//        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));
//        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-phasechange-knobs.madx"));
//
//        // 2025 injection optics for PPLP ramp in steps (to have non-ATS knob)
//        builder.addOptic("R2025aRP_A11mC11mA10mL10m_nonATSKnobs", modelFile("strengths/ATS_Nominal/2025/ats_11m.madx"));
//
//        return builder.build();
//    }

    private OpticDefinitionSet createLowBetaOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-ats.madx"));        
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-phasechange-knobs.madx"));

        // 2025 optics
        builder.addOptic("R2025aRP_A11mC11mA10mL10m", modelFile("strengths/ATS_Nominal/2025/ats_11m.madx"));
        builder.addOptic("R2025aRP_A10mC10mA10mL10m", modelFile("strengths/ATS_Nominal/2025/ats_10m.madx"));
        builder.addOptic("R2025aRP_A970cmC970cmA10mL970cm", modelFile("strengths/ATS_Nominal/2025/ats_970cm.madx"));
        builder.addOptic("R2025aRP_A930cmC930cmA10mL930cm", modelFile("strengths/ATS_Nominal/2025/ats_930cm.madx"));
        builder.addOptic("R2025aRP_A880cmC880cmA10mL880cm", modelFile("strengths/ATS_Nominal/2025/ats_880cm.madx"));
        builder.addOptic("R2025aRP_A810cmC810cmA10mL810cm", modelFile("strengths/ATS_Nominal/2025/ats_810cm.madx"));
        builder.addOptic("R2025aRP_A700cmC700cmA10mL700cm", modelFile("strengths/ATS_Nominal/2025/ats_700cm.madx"));
        builder.addOptic("R2025aRP_A600cmC600cmA10mL600cm", modelFile("strengths/ATS_Nominal/2025/ats_600cm.madx"));
        builder.addOptic("R2025aRP_A510cmC510cmA10mL510cm", modelFile("strengths/ATS_Nominal/2025/ats_510cm.madx"));
        builder.addOptic("R2025aRP_A440cmC440cmA10mL440cm", modelFile("strengths/ATS_Nominal/2025/ats_440cm.madx"));
        builder.addOptic("R2025aRP_A370cmC370cmA10mL370cm", modelFile("strengths/ATS_Nominal/2025/ats_370cm.madx"));
        builder.addOptic("R2025aRP_A310cmC310cmA10mL310cm", modelFile("strengths/ATS_Nominal/2025/ats_310cm.madx"));
        builder.addOptic("R2025aRP_A250cmC250cmA10mL250cm", modelFile("strengths/ATS_Nominal/2025/ats_250cm.madx"));

        builder.addOptic("R2025aRP_A200cmC200cmA10mL200cm_1", modelFile("strengths/ATS_Nominal/2025/ats_200cm.madx"));
        builder.addOptic("R2025aRP_A200cmC200cmA10mL200cm_0-9", modelFile("strengths/ATS_Nominal/2025/ats_200cm_0-9.madx"));
        builder.addOptic("R2025aRP_A200cmC200cmA10mL200cm_0-8", modelFile("strengths/ATS_Nominal/2025/ats_200cm_0-8.madx"));
        builder.addOptic("R2025aRP_A200cmC200cmA10mL200cm_0-7", modelFile("strengths/ATS_Nominal/2025/ats_200cm_0-7.madx"));
        builder.addOptic("R2025aRP_A200cmC200cmA10mL200cm_0-6", modelFile("strengths/ATS_Nominal/2025/ats_200cm_0-6.madx"));
        builder.addOptic("R2025aRP_A200cmC200cmA10mL200cm_0-5", modelFile("strengths/ATS_Nominal/2025/ats_200cm_0-5.madx"));

        builder.addOptic("R2025aRP_A156cmC156cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_156cm.madx"));
        builder.addOptic("R2025aRP_A120cmC120cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_120cm.madx"));
        builder.addOptic("R2025aRP_A112cmC112cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_112cm.madx"));
        builder.addOptic("R2025aRP_A105cmC105cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_105cm.madx"));
        builder.addOptic("R2025aRP_A99cmC99cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_99cm.madx"));
        builder.addOptic("R2025aRP_A93cmC93cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_93cm.madx"));
        builder.addOptic("R2025aRP_A87cmC87cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_87cm.madx"));
        builder.addOptic("R2025aRP_A82cmC82cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_82cm.madx"));
        builder.addOptic("R2025aRP_A77cmC77cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_77cm.madx"));
        builder.addOptic("R2025aRP_A72cmC72cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_72cm.madx"));
        builder.addOptic("R2025aRP_A68cmC68cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_68cm.madx"));
        builder.addOptic("R2025aRP_A64cmC64cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_64cm.madx"));
        builder.addOptic("R2025aRP_A60cmC60cmA10mL200cm", modelFile("strengths/ATS_Nominal/2025/ats_60cm.madx"));
        builder.addOptic("R2025aRP_A54cmC54cmA10mL200cm_Flat", modelFile("strengths/ATS_Nominal/2025/ats_54cm.madx"));
        builder.addOptic("R2025aRP_A49cmC49cmA10mL200cm_Flat", modelFile("strengths/ATS_Nominal/2025/ats_49cm.madx"));
        builder.addOptic("R2025aRP_A44cmC44cmA10mL200cm_Flat", modelFile("strengths/ATS_Nominal/2025/ats_44cm.madx"));
        builder.addOptic("R2025aRP_A40cmC40cmA10mL200cm_Flat", modelFile("strengths/ATS_Nominal/2025/ats_40cm.madx"));
        builder.addOptic("R2025aRP_A36cmC36cmA10mL200cm_Flat", modelFile("strengths/ATS_Nominal/2025/ats_36cm.madx"));
        builder.addOptic("R2025aRP_A33cmC33cmA10mL200cm_Flat", modelFile("strengths/ATS_Nominal/2025/ats_33cm.madx"));
        builder.addOptic("R2025aRP_A30cmC30cmA10mL200cm_Flat", modelFile("strengths/ATS_Nominal/2025/ats_30cm.madx"));
        builder.addOptic("R2025aRP_A27cmC27cmA10mL200cm_Flat", modelFile("strengths/ATS_Nominal/2025/ats_27cm.madx"));
        builder.addOptic("R2025aRP_A24cmC24cmA10mL200cm_Flat", modelFile("strengths/ATS_Nominal/2025/ats_24cm.madx"));
        builder.addOptic("R2025aRP_A22cmC22cmA10mL200cm_Flat", modelFile("strengths/ATS_Nominal/2025/ats_22cm.madx"));
        builder.addOptic("R2025aRP_A20cmC20cmA10mL200cm_Flat", modelFile("strengths/ATS_Nominal/2025/ats_20cm.madx"));
        builder.addOptic("R2025aRP_A18cmC18cmA10mL200cm_Flat", modelFile("strengths/ATS_Nominal/2025/ats_18cm.madx"));

        return builder.build();
    }

    private OpticDefinitionSet createIonOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));

        // 2025 IONS optics
        builder.addOptic("R2025iRP_A11mC11mA10mL10m", modelFile("strengths/ATS_Nominal/2025_IONS/11m.madx"));
        builder.addOptic("R2025iRP_A970cmC970cmA970cmL970cm", modelFile("strengths/ATS_Nominal/2025_IONS/970cm.madx"));
        builder.addOptic("R2025iRP_A920cmC920cmA920cmL920cm", modelFile("strengths/ATS_Nominal/2025_IONS/920cm.madx"));
        builder.addOptic("R2025iRP_A850cmC850cmA850cmL850cm", modelFile("strengths/ATS_Nominal/2025_IONS/850cm.madx"));
        builder.addOptic("R2025iRP_A760cmC760cmA760cmL760cm", modelFile("strengths/ATS_Nominal/2025_IONS/760cm.madx"));
        builder.addOptic("R2025iRP_A670cmC670cmA670cmL670cm", modelFile("strengths/ATS_Nominal/2025_IONS/670cm.madx"));
        builder.addOptic("R2025iRP_A590cmC590cmA590cmL590cm", modelFile("strengths/ATS_Nominal/2025_IONS/590cm.madx"));
        builder.addOptic("R2025iRP_A520cmC520cmA520cmL520cm", modelFile("strengths/ATS_Nominal/2025_IONS/520cm.madx"));
        builder.addOptic("R2025iRP_A450cmC450cmA450cmL450cm", modelFile("strengths/ATS_Nominal/2025_IONS/450cm.madx"));
        builder.addOptic("R2025iRP_A400cmC400cmA400cmL400cm", modelFile("strengths/ATS_Nominal/2025_IONS/400cm.madx"));
        builder.addOptic("R2025iRP_A360cmC360cmA360cmL360cm", modelFile("strengths/ATS_Nominal/2025_IONS/360cm.madx"));
        builder.addOptic("R2025iRP_A320cmC320cmA320cmL320cm", modelFile("strengths/ATS_Nominal/2025_IONS/320cm.madx"));
        builder.addOptic("R2025iRP_A290cmC290cmA290cmL290cm", modelFile("strengths/ATS_Nominal/2025_IONS/290cm.madx"));
        builder.addOptic("R2025iRP_A230cmC230cmA230cmL230cm", modelFile("strengths/ATS_Nominal/2025_IONS/230cm.madx"));
        builder.addOptic("R2025iRP_A185cmC185cmA185cmL185cm", modelFile("strengths/ATS_Nominal/2025_IONS/185cm.madx"));
        builder.addOptic("R2025iRP_A135cmC135cmA135cmL150cm", modelFile("strengths/ATS_Nominal/2025_IONS/135cm.madx"));
        builder.addOptic("R2025iRP_A100cmC100cmA100cmL150cm", modelFile("strengths/ATS_Nominal/2025_IONS/100cm.madx"));
        builder.addOptic("R2025iRP_A82cmC82cmA82cmL150cm", modelFile("strengths/ATS_Nominal/2025_IONS/82cm.madx"));
        builder.addOptic("R2025iRP_A68cmC68cmA68cmL150cm", modelFile("strengths/ATS_Nominal/2025_IONS/68cm.madx"));
        builder.addOptic("R2025iRP_A57cmC57cmA57cmL150cm", modelFile("strengths/ATS_Nominal/2025_IONS/57cm.madx"));
        builder.addOptic("R2025iRP_A50cmC50cmA50cmL150cm", modelFile("strengths/ATS_Nominal/2025_IONS/50cm_150cm.madx"));
        builder.addOptic("R2025iRP_A50cmC50cmA50cmL137m", modelFile("strengths/ATS_Nominal/2025_IONS/50cm_137cm.madx"));
        builder.addOptic("R2025iRP_A50cmC50cmA50cmL127m", modelFile("strengths/ATS_Nominal/2025_IONS/50cm_127cm.madx"));
        builder.addOptic("R2025iRP_A50cmC50cmA50cmL100cm", modelFile("strengths/ATS_Nominal/2025_IONS/50cm_100cm.madx"));

        return builder.build();
    }

}