from pathlib import Path

from acc_models_xsuite import *
import xtrack as xt

_basedir = Path(__file__).parent.parent

def extract_knob(env, var):
    pass # TODO


class LhcOptics(Optics):
    def __init__(self, name, env):
        self.name = name
        self.env = env

    def get_knob_factors(self):
        return {
            "LHCBEAM1/QH_TRIM": extract_knob(self.env, var="dQx.b1"),
            "LHCBEAM1/QV_TRIM": extract_knob(self.env, var="dQx.b1"),
        }

    def get_parameter_values(self):
        tw = self.env.b1.twiss()
        return {
            "LHCBEAM1/BX-IR1": tw["betx", "ip1"]
        }

    def get_twiss(self):
        return {"lhcb1": self.env.b1.twiss(strenghts=True),
                "lhcb2": self.env.b2.twiss(strenghts=True)}

    def get_lines(self):
        return {"lhcb1": self.env.b1,
                "lhcb2": self.env.b2}

    def get_strengths(self):
        return {vn: self.env[vn] for vn in self.env.vars.keys()}

    def set_knobs(self, values):
        raise NotImplementedError()

    def set_strengths(self, values):
        raise NotImplementedError()

class LhcModel(Model):
    def get_optics_names(self):
        return _basedir.glob("strengths/*.json")

    def get_optics(self, optics_name):
        env = xt.Environment.from_json(_basedir / "xsuite" / "lhc.json")
        env.vars.load_json(_basedir / "strengths" / f"{optics_name}.json")
        return LhcOptics(optics_name, env)

    def get_zones(self):
        return [
            SequenceMapping(
                sequence_name="lhcb1",
                accelerator="LHC",
                particle_transfer="LHCRING",
                accelerator_zones=[
                    AcceleratorZoneMapping(name="LHC", beam=1),
                ],
            ),
            SequenceMapping(
                sequence_name="lhcb2",
                accelerator="LHC",
                particle_transfer="LHCRING",
                accelerator_zones=[
                    AcceleratorZoneMapping(name="LHC", beam=2),
                ],
            ),
        ]


models = [LhcModel()]
